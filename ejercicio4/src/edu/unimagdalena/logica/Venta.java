package edu.unimagdalena.logica;

public class Venta {
	private String fecha;
	private Vendedor nroVendedor;
	private Producto producto;
	private int cantidad;
	private String formaPago;
	
	
	public Venta(){
	}

	public Venta(String fecha, Vendedor nroVendedor, Producto producto,
			int cantidad, String formaPago) {
		this.fecha = fecha;
		this.nroVendedor = nroVendedor;
		this.producto = producto;
		this.cantidad = cantidad;
		this.formaPago = formaPago;
	}
	
	
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Vendedor getNroVendedor() {
		return nroVendedor;
	}

	public void setNroVendedor(Vendedor nroVendedor) {
		this.nroVendedor = nroVendedor;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}
	
	
	
	

}

