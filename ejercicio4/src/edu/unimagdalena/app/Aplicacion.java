package edu.unimagdalena.app;

import java.util.ArrayList;

import edu.unimagdalena.logica.Producto;
import edu.unimagdalena.logica.Vendedor;
import edu.unimagdalena.logica.Venta;



public class Aplicacion {
	static Vendedor idVendedor;
   
	static ArrayList<Venta> rVentas = new ArrayList<Venta> ();
	static ArrayList<Vendedor> rVendedor = new ArrayList<Vendedor> ();
	
	static Vendedor vendedor1 = new Vendedor(1);
	static Vendedor vendedor2 = new Vendedor(2);
	static Vendedor vendedor3 = new Vendedor(3);
	static Vendedor vendedor4 = new Vendedor(4);
	
		
	static public void agregarVenta(Venta a){
		rVentas.add(a);
	}
	
	public static void main(String[] args) {
        Producto producto1 = new Producto (1011,"pc",12.99);
        Producto producto2 = new Producto (1012,"laptop",64.99);
        Producto producto3 = new Producto (1013,"mause",80.12);
        Producto producto4 = new Producto (1014,"tablet",845.99);
        
        rVendedor.add(vendedor1);
        rVendedor.add(vendedor2);
        rVendedor.add(vendedor3);
        rVendedor.add(vendedor4);

		Venta venta1 = new Venta("03/09/2016",vendedor1,producto1,1,"Efectivo");
		agregarVenta(venta1);
		
		Venta venta2 = new Venta("02/09/2016",vendedor2,producto2,2,"Debito");
		agregarVenta(venta2);
		
		
		Venta venta3 = new Venta("03/08/2016",vendedor3,producto3,2,"Debito");
		agregarVenta(venta3);
		
		Venta venta4 = new Venta("04/05/2016",vendedor3,producto4,1,"Debito");
		agregarVenta(venta4);
		
		Venta venta5 = new Venta("23/2/2016",vendedor4,producto3,2,"Tarjeta");
		agregarVenta(venta5);
		
		Venta venta6 = new Venta("22/07/2016",vendedor4,producto4,2,"Tarjeta");
		agregarVenta(venta6);
		
		
		System.out.println("EL vendedor que mas vendio fue el numero "+mayorNroVentas().getnVendedor());
		
		imprimirListadoOrdenado();
		
		System.out.println("\nEl monto total de ventas realizadas es: "+montoTotalVentas());
		
		ventaMayorTargeta();
		
	}
	
	static public Vendedor mayorNroVentas(){
	int sumCantidad=0;
	int mayor=0;
	int idVendedor=0;
	 int [] a = new int [rVendedor.size()];
		
		for (int i=0 ;i<rVendedor.size();i++){
			sumCantidad=0;
			for (int j=0;j<rVentas.size();j++){
				if (rVentas.get(j).getNroVendedor()==rVendedor.get(i)){
					sumCantidad=sumCantidad+rVentas.get(j).getCantidad();		
				}
				a[i]=sumCantidad;	
			}	
		}
		for (int x=0; x<rVendedor.size(); x++){
			if (a[x]>mayor){
				mayor=a[x];
				idVendedor=x;
			}
		}
		return rVendedor.get(idVendedor);
	}
	
	static public void imprimirListadoOrdenado(){
		System.out.println("\nLista Vendedores con ventas pagadas en forma Debito :");
		
		for (int s=0; s<rVentas.size(); s++){
			if (rVentas.get(s).getFormaPago()=="Debito"){
				System.out.print("Vendedor N� "+rVentas.get(s).getNroVendedor().getnVendedor());
				System.out.println("   Precio final de la venta: "+rVentas.get(s).getProducto().getPrecio());		
			}
		}
	}
	
	static public double montoTotalVentas(){
		double monto=0;
		for (int m=0; m<rVentas.size(); m++){
			monto = monto + (rVentas.get(m).getProducto().getPrecio() * rVentas.get(m).getCantidad());
		}
		return monto;
	}
	
	static public void ventaMayorTargeta(){
		System.out.println("\nEsta es la venta de mayor importe con Tarjeta de credito:");
		double mayor=0;
		int posicion=-1;
		
		for (int t=0; t<rVentas.size(); t++){
			
			if (rVentas.get(t).getFormaPago()=="Tarjeta"){
				if (rVentas.get(t).getProducto().getPrecio() > mayor){
					mayor = rVentas.get(t).getProducto().getPrecio();
					posicion = t;
				}
				
			}
		}
		System.out.println("Fecha: "+rVentas.get(posicion).getFecha()+
								"\nVendedor N�: "+rVentas.get(posicion).getNroVendedor().getnVendedor()+
								"\nProducto: "+rVentas.get(posicion).getProducto().getDescripcion()+
								" - Cantidad: "+rVentas.get(posicion).getCantidad()+
								" - Precio unitario: "+rVentas.get(posicion).getProducto().getPrecio()+
								"\nForma de pago: "+rVentas.get(posicion).getFormaPago());
	} 
	
		
	

}
